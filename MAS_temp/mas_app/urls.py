"""URLconf"""
from django.urls import path
from . import views


urlpatterns = [
    path('', views.login, name='login'),
    path('search', views.search, name='search'),
    path('add_contracts', views.add_contracts, name='add_contracts'),
    path('more', views.more, name="more"),
    path('delete', views.delete, name="delete"),
    # path('summarize_sales', views.summarize_sales, name="summarize_sales"),
]
