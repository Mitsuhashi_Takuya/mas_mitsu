const user_id = 'admin';
const password = 'hogehoge';
$(function () {
    let idfield = $("input[name=user_id]");//入力されたID
    let passwordfield = $("input[name=password]");//入力されたパスワード
    $('#btn_id').click(function () {
        // 最低限のバリデーション
        if (idfield.val() == "") {//IDの入力確認
            $("#output").addClass("alert alert-danger animated fadeInUp").html("IDを入力してください　");
            return false
        }
        if (passwordfield.val() == "") {//パスワードの入力確認
            $("#output").addClass("alert alert-danger animated fadeInUp").html("パスワードを入力してください　");
            return false
        }
        if ((idfield.val() == user_id) && (passwordfield.val() == password)) {//登録内容と一致しているか確認
            $('#login_form').submit();
        } else {
            $("#output").addClass("alert alert-danger animated fadeInUp").html("ユーザ名かパスワードが間違っています　");
            return false
        };
    }
    )
})