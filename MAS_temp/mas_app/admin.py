"""adminサイトに表示する項目"""
from django.contrib import admin
from mas_app.models import Customer_T, MAS_contract_T, Plan


class ContractAdmin(admin.ModelAdmin):
    """データ"""
    list_display = ('customer_id', 'plan_name', 'early_stage_cost', 'current_month_charge',
                    'start_date', 'modification_date', 'canceling_date', 'delete_flag')


class CustomerAdmin(admin.ModelAdmin):
    """Customerデータ"""


class PlanAdmin(admin.ModelAdmin):
    """Planデータ"""


admin.site.register(MAS_contract_T, ContractAdmin)
admin.site.register(Customer_T, CustomerAdmin)
admin.site.register(Plan, PlanAdmin)
