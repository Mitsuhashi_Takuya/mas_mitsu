# Generated by Django 2.0.4 on 2019-05-29 08:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mas_app', '0014_contract_is_deleted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='contact_phone_TEL',
            field=models.IntegerField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='contract',
            name='represent_TEL',
            field=models.IntegerField(blank=True, max_length=15, null=True),
        ),
    ]
