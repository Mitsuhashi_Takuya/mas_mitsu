"""mas_app.views"""
from django.shortcuts import render, HttpResponse
from django.db.models import Q  # データベースの検索用
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger  # ページネーション用
from mas_app.models import Customer_T, MAS_contract_T, Plan
import MAS_temp
import json
import datetime


def paginate_query(request, queryset, count, key):
    """ページネーション用に、Pageオブジェクトを返す。"""
    paginator = Paginator(queryset, count)
    # page = request.GET.get('page')

    try:
        page_obj = paginator.page(key)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    return page_obj


def more(request):
    """新しい顧客番号をかぶりなく生成する"""
    print('more_entered')

    modal_select_date = request.POST["modal_select_date"].translate(
        str.maketrans({'年': '-', '月': None, '日': None}))  # 1999年01月01日→1999-0101
    modal_select_plan = request.POST["modal_select_plan"]

    # 顧客番号の前半部分(S-2019-0402-など)
    head = modal_select_plan + "-" + modal_select_date + "-"

    # かぶりのない顧客番号を探す
    tail = ""  # 顧客番号の末尾(02など)
    index = 1
    while True:
        if index < 10:
            tail = '0'+str(index)
        else:
            tail = str(index)
        temp_id = head+tail
        print(temp_id)
        # すでにある顧客番号か確認
        if Contract.objects.filter(customer_id=temp_id).exists():
            print("Entry contained in queryset")
            index += 1
        else:
            new_contract_id = temp_id
            break

    return HttpResponse(new_contract_id)


def login(request):
    """ログイン画面の表示"""
    return render(
        request,
        'login.html'
    )


def search(request):
    """検索画面、検索結果の表示"""
    print('search')
    # プラン選択セレクトボックスの項目用
    plans = Plan.objects.all().order_by('plan_name')
    # customer_ids = Contract.objects.all().customer_id  # 登録済み顧客番号
    try:
        customer_name = request.POST['search_customer_name']
        client_phone = request.POST["search_client_phone"]
        plan_names = request.POST.getlist('search_plan_name')
        search_month = request.POST['search_month']
    except KeyError:  # 検索条件がない(ログインして初めて来た)なら検索画面に飛ばす
        return render(
            request,
            'search.html',
            {'plans': plans,
             'number_of_hit': 0}
        )

    if (not customer_name)and(not client_phone)and(not plan_names)and(not search_month):  # 検索条件が空(空欄でPOST)なら検索画面に飛ばす
        return render(
            request,
            'search.html',
            {'plans': plans,
             'number_of_hit': 0}
        )

    # ここに来るなら検索を開始できる
    print("検索開始")

    checked = request.POST["show_deleted"]  # checked=削除済みの契約も表示
    if checked == 'checked':
        show_deleted = True
    else:
        show_deleted = False
    print(show_deleted)

    # 得たクエリセットをどう加工するか
    # searchなら結合してそのまま出力
    # summarize_salesなら売上の集計と個別の表の出力
    do_what = request.POST["do_what"]
    print(do_what)
    if do_what == 'summarize_sales':  # 売上集計では検索時点で解約されているものも集計対象
        checked = 'checked'
        show_deleted = True

    contracts = Contract.objects.none()  # 空のクエリセット
    if customer_name:  # 顧客名が空でないなら顧客名で検索
        print("顧客名で検索")
        if show_deleted:  # 削除済みの契約も表示
            contracts = Contract.objects.filter(
                customer_name__icontains=customer_name)  # 部分一致（大文字小文字区別無し）検索
        else:  # 削除されていない契約を表示
            contracts = Contract.objects.filter(
                customer_name__icontains=customer_name, is_deleted=False)  # 部分一致（大文字小文字区別無し）検索

    contracts2 = Contract.objects.none()  # 空のクエリセット
    if client_phone:  # 顧客電話番号が空でないなら顧客電話番号で検索
        print("顧客電話番号で検索")
        if show_deleted:  # 削除済みの契約も表示
            contracts2 = Contract.objects.filter(
                Q(represent_TEL__icontains=client_phone) | Q(
                    contact_phone_TEL__icontains=client_phone)
            )
        else:  # 削除されていない契約を表示
            contracts2 = Contract.objects.filter(
                Q(represent_TEL__icontains=client_phone, is_deleted=False) | Q(
                    contact_phone_TEL__icontains=client_phone, is_deleted=False)
            )

    contracts3 = Contract.objects.none()  # 空のクエリセット
    if plan_names:  # プラン名が空でないならプラン名で検索
        print("プラン名で検索")
        if show_deleted:  # 削除済みの契約も表示
            contracts3 = Contract.objects.filter(plan_name__in=plan_names)
        else:  # 削除されていない契約を表示
            contracts3 = Contract.objects.filter(
                plan_name__in=plan_names, is_deleted=False)

    contracts4 = Contract.objects.none()  # 空のクエリセット
    if search_month:  # 月が空でないなら月で検索
        print("月で検索")
        date_str = search_month + "01日"
        print(date_str)

        search_datetime = datetime.datetime.strptime(date_str, "%Y年%m月%d日")
        # 開始月が検索条件以前かつ(解約日がNullまたは検索条件より後)の契約を抽出
        contracts4 = Contract.objects.filter(
            Q(start_date__lte=search_datetime, canceling_date__isnull=True) |
            Q(start_date__lte=search_datetime, canceling_date__gte=search_datetime)
        )

    print('クエリセットの結合')
    # 検索条件が空欄だった条件のクエリセットを集計の邪魔にならないよう加工
    if not customer_name:
        contracts = Contract.objects.all()
    if not client_phone:
        contracts2 = Contract.objects.all()
    if not plan_names:
        contracts3 = Contract.objects.all()
    if not search_month:
        contracts4 = Contract.objects.all()
    contracts = contracts & contracts2 & contracts3 & contracts4  # 表示する対象のクエリセット

    print(contracts)
    if do_what == 'search':  # 検索の場合
        summary = {}  # 売上の集計は行わないのでtemplateに空の辞書を渡す
    else:  # 売上集計の場合
        if not plan_names:  # プラン名が空なら売上集計の対象は全プランにする
            plans = Plan.objects.all()
            for plan in plans:
                plan_names.append(plan.plan_name)
        if not search_month:  # 月が空なら売上集計の対象は実行時の月の1日の0時に設定する
            today = datetime.datetime.today()
            month = today.replace(day=1, hour=0, minute=0,
                                  second=0, microsecond=0)
            # 検索条件を検索結果ページに表示するために形式を整える
            search_month = month.strftime('%Y{0}%m{1}').format(
                *'年月')
        else:  # 指定があれば検索可能な形に加工する
            month = datetime.datetime.strptime(
                search_month, "%Y年%m月")

        # 関数にクエリセットを渡してプランごとの契約件数、初期費用の和、当月料金の和が入った辞書を返す処理
        summary = summarize_sales(contracts, plan_names, month)
        print(summary)

    if (contracts):  # ヒットした契約の件数
        number_of_hit = contracts.count()
    else:
        number_of_hit = 0
    print(number_of_hit)
    try:
        sort = request.POST["sort"]
    except KeyError:  # ソート条件が空ならプランでソート
        sort = 'plan_name'
    contracts = contracts.order_by(sort)

    try:
        key = request.POST['key']
    except KeyError:  # ページ指定が空なら1ページ目を表示するよう設定
        key = 1

    page_obj = paginate_query(
        request, contracts, MAS_temp.settings.PAGE_PER_ITEM, key)   # ページネーション

    dic = {  # モデルから取得したobjectsの代わりに、page_objを渡す
        'customer_name': customer_name,
        'client_phone': client_phone,
        'page_obj': page_obj,
        'contracts': page_obj.object_list,
        'plans': plans,
        'sort': sort,
        'key': key,
        'checked': checked,
        'number_of_hit': number_of_hit,
        'selected_plan_names': plan_names,
        'search_month': search_month,
        'do_what': do_what,
        'summary': summary,
    }
    # print(page_obj.object_list[0].plan_name)
    return render(
        request,
        'search.html',
        dic
    )


def summarize_sales(contracts, plan_names, search_month):
    """渡された契約について、プランごとの売上集計結果を返す関数"""
    # print(search_month)
    tempdict = {}
    sum_early_stage_cost = 0  # 検索プラン全体の初期費用合計
    sum_sales = 0  # 検索プラン全体の売上合計
    sum_start_contract = 0  # 検索プラン全体の課金開始件数
    sum_total_contract = 0  # 検索プラン全体の累計件数

    for i in plan_names:
        tempquery = contracts.filter(plan_name=i)  # あるプランの契約のクエリセット
        # print(i)
        # print(tempquery)
        early_stage_cost = 0  # 初期費用合計
        sales = 0  # 当月料金合計
        start_contract = 0  # 課金開始件数
        total_contract = 0  # 累計契約件数
        for q in tempquery:
            # print(q.start_date)
            if search_month == q.start_date:
                print('一致')
                early_stage_cost += q.early_stage_cost  # その契約の初期費用を合計に足す
                sales += q.early_stage_cost + \
                    q.current_month_charge  # 初期費用と当月料金を合計に足す
            else:
                sales += q.current_month_charge
            total_contract += 1
            if(q.start_date == search_month):
                start_contract += 1

        sum_early_stage_cost += early_stage_cost
        sum_sales += sales
        sum_start_contract += start_contract
        sum_total_contract += total_contract

        tempdict[i] = {
            'plan_name': i,
            'start_contract': start_contract,
            'total_contract': total_contract,
            'early_stage_cost': early_stage_cost,
            'sales':  sales,
        }
    tempdict['合計'] = {
        'plan_name': '合計',
        'start_contract':  sum_start_contract,
        'total_contract': sum_total_contract,
        'early_stage_cost': sum_early_stage_cost,
        'sales':  sum_sales,
    }

    return tempdict


def add_contracts(request):
    """契約の追加、編集"""
    array = []  # Contractクラスのフィールドのリスト
    contract = Contract()
    for key, value in contract.__dict__.items():
        array.append(key)
        # print(key)
    array.remove('_state')
    # array.remove('remote_way')
    # array.remove('remote_connection')
    # array.remove('remote_local')
    # array.remove('perimter_account')
    # array.remove('perimter_network')
    array.remove('is_deleted')

    newcontract = {}
    for i in array:

        if i in ['start_date', 'modification_date', 'canceling_date']:  # 文字列を日付に変換
            date_str = request.POST['modal_' + i]+"01日"
            try:
                newcontract[i] = datetime.datetime.strptime(
                    date_str, "%Y年%m月%d日")

            except ValueError:
                print("error")
                newcontract[i] = ''
        elif i in ['early_stage_cost', 'current_month_charge']:  # 文字列を整数に変換
            try:
                cost_str = request.POST['modal_' + i]
                newcontract[i] = int(cost_str)
            except ValueError:
                newcontract[i] = 0
                # return render(
                #     request,
                #     'errinput.html',
                #     {'message': '初期費用、当月料金には数値を入力してください'}
                # )
        elif i in ['represent_TEL', 'contact_phone_TEL']:  # 文字列を整数に変換
            try:
                TEL_str = request.POST['modal_' + i]
                newcontract[i] = int(TEL_str)
            except ValueError:
                newcontract[i] = 0
        else:
            str = request.POST['modal_' + i]
            newcontract[i] = str.replace(' ', '').replace('　', '')
        print(i)
        print(newcontract[i])

    olddata = Contract.objects.filter(
        customer_id=newcontract['customer_id']).first()
    print(array[0])
    index = 0
    contract.customer_id = newcontract['customer_id']

    contract.customer_name = newcontract['customer_name']

    contract.plan_name = newcontract['plan_name']

    if newcontract['start_date']:  # 有効な日付の入力があるか確認、なければデータベースに入れない
        contract.start_date = newcontract['start_date']
    if newcontract['modification_date']:  # 有効な日付の入力があるか確認、なければデータベースに入れない
        contract.modification_date = newcontract['modification_date']
    if newcontract['canceling_date']:  # 有効な日付の入力があるか確認、なければデータベースに入れない
        contract.canceling_date = newcontract['canceling_date']
    contract.represent_TEL = newcontract['represent_TEL']
    contract.contact_phone_TEL = newcontract['contact_phone_TEL']
    contract.person_in_first_charge = newcontract['person_in_first_charge']
    contract.first_name_1 = newcontract['first_name_1']
    contract.contacts_email_1 = newcontract['contacts_email_1']
    contract.person_in_second_charge = newcontract['person_in_second_charge']
    contract.first_name_2 = newcontract['first_name_2']
    contract.contacts_email_2 = newcontract['contacts_email_2']
    # contract.manufacturer = newcontract['manufacturer']
    # contract.model_name = newcontract['model_name']
    # contract.model_number = newcontract['model_number']
    # contract.serial_number = newcontract['serial_number']
    contract.startia_charge = newcontract['startia_charge']
    contract.startia_engineer = newcontract['startia_engineer']
    contract.early_stage_cost = newcontract['early_stage_cost']
    contract.current_month_charge = newcontract['current_month_charge']
    contract.note = newcontract['note']

    # 削除処理の場合はデリートフラグを立てる
    is_deleted = request.POST['delete']
    if is_deleted:
        contract.is_deleted = True  # デリートフラグを立てる

    contract.save()

    # プラン選択セレクトボックスの項目用
    plans = Plan.objects.all().order_by('plan_name')

    toast = "データベースを更新しました"
    dict = {
        'plans': plans,

        'toast': toast,
    }
    return render(
        request,
        'search.html',
        dict
    )


def delete(request):
    target_id = request.POST['modal_customer_id']
    print(target_id)
    target_contract = Contract.objects.filter(
        customer_id=target_id).first()  # 部分一致（大文字小文字区別無し）検索
    target_contract.is_deleted = True  # デリートフラグを立てる
    target_contract.save()

    # プラン選択セレクトボックスの項目用
    plans = Plan.objects.all().order_by('plan_name')

    toast = "削除しました"
    dict = {
        'plans': plans,

        'toast': toast,
    }
    return render(
        request,
        'search.html',
        dict
    )
