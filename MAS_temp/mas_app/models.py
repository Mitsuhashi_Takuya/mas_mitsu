"""モデル置き場"""
from django.db import models


class Plan(models.Model):
    """契約プラン一覧"""
    plan_name = models.CharField(
        max_length=5, primary_key=True)  # 顧客名


# 顧客データ
class Customer_T(models.Model):
    customer_id = models.CharField('顧客番号', max_length=15)
    customer_name = models.CharField('顧客名', max_length=50)  # 顧客名
    # 「YubinBango api」or「Google Maps API」
    post_code = models.CharField('郵便番号', max_length=8)
    prefectures = models.CharField('都道府県', max_length=5)
    address = models.CharField('市区町村番地', max_length=40)
    address2 = models.CharField('建物名', max_length=40, null=True, blank=True)
    first_charge = models.CharField('担当者', max_length=20)
    second_charge = models.CharField(
        '担当者2', max_length=20, null=True, blank=True)
    represent_TEL = models.IntegerField('代表TEL', max_length=15)
    contacts_TEL = models.IntegerField(
        '連絡用TEL', max_length=15, null=True, blank=True)
    contacts_email1 = models.EmailField('連絡用メールアドレス1', max_length=127)
    contacts_email2 = models.EmailField('連絡用メールアドレス2', max_length=127)
    contacts_email3 = models.EmailField('連絡用メールアドレス3', max_length=127)
    startia_charge = models.CharField('担当営業', max_length=20)
    startia_engineer = models.CharField('担当エンジニア', max_length=20)
    delete_flag = models.BooleanField('終了・削除フラグ', default=False)
    note = models.TextField('備考', max_length=500, null=True, blank=True)


# MAS契約情報用テーブル
class MAS_contract_T(models.Model):
    """契約のモデルクラス"""
    customer_id = models.ForeignKey(
        'mas.customer_T', verbose_name='契約会社', on_delete=models.CASCADE)  # 顧客番号
    plan_name = models.CharField('プラン', max_length=20)  # 契約プラン
    start_date = models.DateField(
        '契約開始日', auto_now=False, auto_now_add=False, null=True, blank=True)  # 課金開始年、月
    modification_date = models.DateField(
        '変更月', auto_now=False, auto_now_add=False, null=True, blank=True)  # 変更月
    canceling_date = models.DateField(
        '解約月', auto_now=False, auto_now_add=False, null=True, blank=True)  # 解約月
    early_stage_cost = models.IntegerField(
        '初期費用', max_length=10, null=True, blank=True)    # 初期費用
    current_month_charge = models.IntegerField(
        '当月料金', max_length=10, null=True, blank=True)   # 当月料金
    note = models.TextField('備考', max_length=500, null=True, blank=True)  # 備考

    delete_flag = models.BooleanField(
        '終了・削除フラグ', default=False)  # 削除フラグ(True=削除状態)


# MASライセンス契約情報用テーブル
class Licensing_agreement_T(models.Model):
    customer_name = models.ForeignKey(
        'mas.customer_T', verbose_name='契約会社', on_delete=models.CASCADE)
    appropriation_flag = models.BooleanField('計上済み', default=False)
    progress = models.CharField('履歴', max_length=15)
    plan_name = models.CharField('プラン', max_length=20)
    start_date = models.DateField(
        '契約開始日', auto_now=False, auto_now_add=False, null=True, blank=True)
    end_date = models.DateField(
        '契約終了日', auto_now=False, auto_now_add=False, null=True, blank=True)
    estimate_before = models.CharField('現行見積もり番号', max_length=20)
    estimate_after = models.CharField('更新後見積もり番号', max_length=20)
    inspection_scheduled_date = models.DateField(
        '検収予定日', auto_now=False, auto_now_add=False)
    inspection_date = models.DateField(
        '検収日', auto_now=False, auto_now_add=False, null=True, blank=True)
    goods_name = models.CharField('製品名', max_length=50)
    equipment_model = models.CharField('機種型番', max_length=50)
    serial_number = models.CharField('シリアルナンバー', max_length=30)
    target_name = models.CharField('対象商品', max_length=15)
    order_count = models.IntegerField('注文数', max_length=10, default=1)
    production_cost = models.IntegerField('原価', max_length=10, default=0)
    sales = models.IntegerField('売上', max_length=10, default=0)
    delete_flag = models.BooleanField('終了・削除フラグ', default=False)
    notice = models.TextField('注意事項', max_length=500, null=True, blank=True)
    note = models.TextField('備考', max_length=500, null=True, blank=True)
